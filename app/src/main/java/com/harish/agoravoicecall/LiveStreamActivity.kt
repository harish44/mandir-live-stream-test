package com.harish.agoravoicecall

import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import io.agora.rtc.Constants
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine

const val PERMISSION_ID = 213
const val VOICE_CHANNEL_NAME = "mandirChannel"


class LiveStreamActivity : AppCompatActivity() {

    private var rtcEngine: RtcEngine? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_stream)

        initAgoraSdk()
        getClient()

        findViewById<CardView>(R.id.end_call).setOnClickListener {
            finish()
        }

        var isMute = false

        findViewById<CardView>(R.id.mute_volume).setOnClickListener {
            val iv = findViewById<ImageView>(R.id.iv_mute)
            if(!isMute) {
                rtcEngine?.muteAllRemoteAudioStreams(true)
                iv.setImageResource(R.drawable.ic_volume)
                isMute=true
            }
            else {
                rtcEngine?.muteAllRemoteAudioStreams(false)
                iv.setImageResource(R.drawable.ic_volume_mute)
                isMute=false
            }
        }

    }

    private fun getClient() {
        val client = intent.getIntExtra("client", 0)
        rtcEngine?.setClientRole(client)

        val textView = findViewById<TextView>(R.id.tv_client)
        if(client==Constants.CLIENT_ROLE_BROADCASTER)
            textView.text = "You are the Broadcaster"
        else
            textView.text = "You are one of the listeners."

        findViewById<TextView>(R.id.tv_channelName).text = "Channel Name = $VOICE_CHANNEL_NAME"
    }

    private fun initAgoraSdk() {
        initAgoraEngine()
        joinChannel()
    }

    private fun initAgoraEngine() {
        try {
            rtcEngine =
                RtcEngine.create(baseContext, getString(R.string.agora_app_id), rtcEventHandler)
        } catch (e: Exception) {
            Log.e("Error", e.message.toString())
        }
    }

    private fun joinChannel() {
        val accessToken: String? = getString(R.string.agora_access_token)

        //Set the channel profile to know what stuff are we gonna use like broadcasting or 1-1 voice call etc
        rtcEngine!!.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING)
        rtcEngine!!.enableWebSdkInteroperability(true)
        rtcEngine!!.joinChannel(accessToken, VOICE_CHANNEL_NAME, "Optional data", 0)
    }

    private fun leaveChannel() {
        rtcEngine!!.leaveChannel()
    }

    override fun onDestroy() {
        super.onDestroy()
        leaveChannel()
        RtcEngine.destroy()
        rtcEngine = null
    }


    private val rtcEventHandler: IRtcEngineEventHandler = object : IRtcEngineEventHandler() {

        //Occurs when a user mutes the audio
        override fun onUserMuteAudio(uid: Int, muted: Boolean) {
            runOnUiThread {
                Toast.makeText(this@LiveStreamActivity, "MUTED", Toast.LENGTH_LONG).show()
            }
        }

        //Occurs when user leaves the channel or goes offline
        override fun onUserOffline(uid: Int, reason: Int) {
            runOnUiThread {
                Toast.makeText(this@LiveStreamActivity, "Offline", Toast.LENGTH_LONG).show()
            }
        }
    }

}