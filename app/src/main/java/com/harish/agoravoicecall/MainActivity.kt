package com.harish.agoravoicecall

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.button.MaterialButton
import io.agora.rtc.Constants
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /**
         * POINTS TO NOTE:
         * 1) The channel name has to be same as what we give in Agora Console.
         *
         * 2) Access token has to be generated from a server. This access token is generated for
         *    testing purpose which will be active for only 24 hours.
         *
         * 3) Apk size might increase drastically.
         *
         * 4) Once the user joins the channel, the user subscribes to the audio streams of all the other users in the channel by default,
         *    giving rise to usage and billing calculation.
         *    If you do not want to subscribe to a specified stream or all remote streams,
         *    call the mute methods accordingly.
         */

        //Request the permission
        //Taken from agora doc

        findViewById<Button>(R.id.btn_joincall).setOnClickListener {
            if (checkSelfPermission(android.Manifest.permission.RECORD_AUDIO, PERMISSION_ID)) {
                alert()
            }
        }

    }

    private fun checkSelfPermission(permission: String, reqCode: Int): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                permission
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                Array<String>(1) { permission },
                reqCode
            )
            return false
        }
        return true
    }

    private fun alert() {
        val builder = AlertDialog.Builder(this)
            .setTitle("Audience or Broadcaster?")
            .setMessage("Do you want to be a audience or a broadcaster?")
            .setPositiveButton("Broadcaster") { _, _ ->
                openLiveStream(Constants.CLIENT_ROLE_BROADCASTER)
            }
            .setNegativeButton("Audience") { _, _ ->
                openLiveStream(Constants.CLIENT_ROLE_AUDIENCE)
            }
        builder.show()
    }

    private fun openLiveStream(clientRole: Int) {
        val intent = Intent(this, LiveStreamActivity::class.java)
        intent.putExtra("client", clientRole)
        startActivity(intent)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_ID -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    alert()
                } else {
                    Toast.makeText(this, "No Permission", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }


}
